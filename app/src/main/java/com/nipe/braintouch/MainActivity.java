package com.nipe.braintouch;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";
    public final static int REQUEST_CODE_OVERLAY = 10;
    public final int REQUEST_CODE = 20;
    private final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN
    };

    private int latestTrained = -1;
    private Button trainPush,trainPull,trainNeutral,scan,profile,startCourse;
    private LinearLayout trainButtonContainer;
    private TextView batteryIcon, batteryLevel, af3_conn,t7_conn,pz_conn,t8_conn,af4_conn,dotLoading;
    private Vector<TextView> conns;
    private Typeface fontAwesomeSolid;
    private Vector<Integer> strenghts;
    private Timer UIUpdateTimer;
    private AlertDialog acceptTrainingDialog;

    private FileStorage fs;
    private BroadcastReceiver receiver;

    private Timer loadingUpdater;
    String loadingProgress = "";

    private static final int REQUEST_CODE_TRAINING_COURSE = 0;

    private static final int HANDLER_UPDATE_HEADSET_CONNECTION = 0;
    private static final int HANDLER_UPDATE_LOADING = 1;
    private static final int HANDLER_STOP_LOADING = 2;

    private static final int TRAIN_PUSH = 1;
    private static final int TRAIN_PULL = 2;
    private static final int TRAIN_NEUTRAL = 0;

    @SuppressLint("HandlerLeak")
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what){
                case HANDLER_UPDATE_HEADSET_CONNECTION:
                    updateBatteryLevel();
                    updateConnectionLevels();
                    break;

                case HANDLER_UPDATE_LOADING:
                    updateLoadingText();
                    break;

                case HANDLER_STOP_LOADING:
                    dotLoading.setText("");
                    loadingUpdater.cancel();
                    //checkTrainingOk();
                    break;
            }
        }
    };

    private void checkTrainingOk() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you satisfied with the training?")
                .setTitle("Training finished")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendTrainingResult(true, latestTrained);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendTrainingResult(false, latestTrained);
                    }
                })
                .setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void checkTrainingFail() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Training failed. Do you want to retry?")
                .setTitle("Training failed")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        HeadsetConnector.startTraining(latestTrained, false);
                        startLoadingAnimation();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendTrainingResult(false, latestTrained);
                    }
                })
                .setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendTrainingResult(boolean accepted, int TRAIN_CODE){
        HeadsetConnector.trainingResult(accepted, TRAIN_CODE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        //initialize database
        fs = FileStorage.getInstance(getApplicationContext());

        fontAwesomeSolid = Typeface.createFromAsset(getAssets(), "fontawesome_free_solid.otf");
        conns = new Vector<>();
        strenghts = new Vector<>();
        loadingUpdater = new Timer();

        IntentFilter filter = new IntentFilter("HEADSET_CONNECTED");
        filter.addAction("PROFILE_SET");
        filter.addAction("TRAINING");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals("HEADSET_CONNECTED")){
                    if(UIUpdateTimer == null){
                        UIUpdateTimer = new Timer();
                        UIUpdateTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.obtainMessage(HANDLER_UPDATE_HEADSET_CONNECTION).sendToTarget();
                            }
                        }, 5000,1500);
                    }
                } else if(intent.getAction().equals("PROFILE_SET")){
                    ((TextView)findViewById(R.id.profileText)).setText(intent.getExtras().getString("data"));
                } else if(intent.getAction().equals("TRAINING")) {
                    if(intent.getExtras().getString("data").equals("SUCCESS")) {
                        trainButtonContainer.setVisibility(View.VISIBLE);
                        handler.obtainMessage(HANDLER_STOP_LOADING).sendToTarget();
                        checkTrainingOk();
                    }
                    else if(intent.getExtras().getString("data").equals("STARTED")) {
                        startLoadingAnimation();
                    }
                    else {
                        handler.obtainMessage(HANDLER_STOP_LOADING).sendToTarget();
                        trainButtonContainer.setVisibility(View.VISIBLE);
                        checkTrainingFail();
                    }
                }
            }
        };
        this.registerReceiver(receiver,filter);

        if(checkCurrentVersionEqualOrGreater(23)){
            if(!Settings.canDrawOverlays(this)) {
                Log.d(TAG,"onCreate - requesting permission");
                Intent intent = new Intent(
                        Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_CODE_OVERLAY);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        start();
    }

    private void start() {
        setContentView(R.layout.activity_main);
        Log.d(TAG,"In start method");

        if(Build.VERSION.SDK_INT < 23 || hasPermissions(PERMISSIONS)){
            Log.d(TAG,"Starting the service");
            Intent svc = new Intent(this, BrainTouchService.class);
            startService(svc);
        }

        //finish();

        List<Profile> profileList = fs.getAllProfiles();
        for(Profile p:profileList)
            Log.d(TAG,"Found persisted profile " + p.getDisplay_name() + " | " + p.getByte_data() + "\n");

        initViews();
    }

    private void initViews() {
        trainNeutral = findViewById(R.id.trainNeutral);
        trainPush = findViewById(R.id.trainPush);
        trainPull = findViewById(R.id.trainPull);
        scan = findViewById(R.id.scanButton);
        profile = findViewById(R.id.selectProfileButton);
        trainButtonContainer = findViewById(R.id.trainingButtonContainer);
        startCourse = findViewById(R.id.startCourseButton);

        batteryIcon = findViewById(R.id.batteryIcon);
        batteryLevel = findViewById(R.id.batteryLevel);

        af3_conn = findViewById(R.id.AF3_connection);
        t7_conn = findViewById(R.id.T7_connection);
        pz_conn = findViewById(R.id.Pz_connection);
        t8_conn = findViewById(R.id.T8_connection);
        af4_conn = findViewById(R.id.AF4_connection);
        dotLoading = findViewById(R.id.dotLoadingText);
        dotLoading.setText("");

        conns.add(af3_conn);
        conns.add(t7_conn);
        conns.add(pz_conn);
        conns.add(t8_conn);
        conns.add(af4_conn);

        for(TextView tv : conns){
            tv.setTypeface(fontAwesomeSolid);
        }

        batteryIcon.setTypeface(fontAwesomeSolid);

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHeadsetDialog();
            }
        });

        trainNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTraining(TRAIN_NEUTRAL);
            }
        });

        trainPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTraining(TRAIN_PUSH);
            }
        });

        trainPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTraining(TRAIN_PULL);
            }
        });

        startCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TrainingCourseActivity.class);
                startActivityForResult(intent, REQUEST_CODE_TRAINING_COURSE);
            }
        });

        /*up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("UP");
            }
        });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("DOWN");
            }
        });
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("LEFT");
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("RIGHT");
            }
        });
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("CLICK");
            }
        });*/
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProfileDialog();
            }
        });
    }

    private void startLoadingAnimation() {
        trainButtonContainer.setVisibility(View.INVISIBLE);
        loadingUpdater = new Timer();
        loadingUpdater.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.obtainMessage(HANDLER_UPDATE_LOADING).sendToTarget();
            }
        }, 0,560);
    }

    private void updateLoadingText(){
        String dotLoadingCurrentText = dotLoading.getText().toString();

        if(dotLoadingCurrentText.length() >= "Training".length()){
            loadingProgress = dotLoadingCurrentText + ".";
        }

        if(dotLoadingCurrentText.length() >= "Training...".length() || dotLoadingCurrentText.length() == 0){
            loadingProgress = "Training";
        }

        dotLoading.setText(loadingProgress);
    }

    private void sendMessage(String message){
        Intent intent = new Intent();
        intent.setAction("BUTTON_PRESSED");
        intent.putExtra("data",message);
        sendBroadcast(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_TRAINING_COURSE) {
                long endTime = data.getExtras().getLong("end_time");
                endTime /= 1000;
                Toast.makeText(getApplicationContext(), "Finished course in " + String.valueOf(endTime) + " seconds", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == REQUEST_CODE_OVERLAY && checkCurrentVersionEqualOrGreater(23)) {
            if (Settings.canDrawOverlays(this)) {
                Log.d(TAG, "Overlay permission granted");
                requestPermissions(PERMISSIONS, REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CODE){
            for(int i = 0; i < grantResults.length; i++){
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    Log.d(TAG, "Not all permissions granted!");
                    return;
                }
            }
            start();
        }
    }

    @SuppressLint("NewApi")
    private boolean hasPermissions(String[] permissions) {
        if(!Settings.canDrawOverlays(this)){
            return false;
        }

        for (int i = 0; i < permissions.length; i++) {
            Log.d(TAG, permissions[i]);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, String.valueOf(i));
                return false;
            }
            Log.d(TAG, "We have permission for: " + permissions[i]);
        }
        return true;
    }

    private void showHeadsetDialog(){
        HeadsetScanDialog dialog = new HeadsetScanDialog(this);
        dialog.show();
    }

    private void showProfileDialog(){
        SelectProfileDialog dialog = new SelectProfileDialog(this);
        dialog.show();
    }

    private void updateBatteryLevel(){
        int chargeLevel = HeadsetConnector.getConnectedDeviceBatteryLevel();
        batteryLevel.setText(String.valueOf(chargeLevel) + "%");
    }

    private void updateConnectionLevels(){
        HeadsetConnector.getConnectedDeviceConnectionLevels(strenghts);
        for(int i = 0; i < strenghts.size(); i++){
            switch (strenghts.get(i)){
                case 0:
                    conns.get(i).setTextColor(Color.parseColor("#000000"));
                    break;
                case 1:
                    conns.get(i).setTextColor(Color.parseColor("#FF0000"));
                    break;
                case 2:
                    conns.get(i).setTextColor(Color.parseColor("#FFA500"));
                    break;
                case 3:
                    conns.get(i).setTextColor(Color.parseColor("#FFFF00"));
                    break;
                case 4:
                    conns.get(i).setTextColor(Color.parseColor("#00FF00"));
                    break;
                default:
                    Log.d(TAG, "updateConnectionLevels: Invalid index: " + i);
            }
        }
        strenghts.clear();
    }

    private boolean checkCurrentVersionEqualOrGreater(int version){
        return Build.VERSION.SDK_INT >= version;
    }

    private void startTraining(int toTrain){
        HeadsetConnector.startTraining(toTrain, false);
        latestTrained = toTrain;
    }
}
