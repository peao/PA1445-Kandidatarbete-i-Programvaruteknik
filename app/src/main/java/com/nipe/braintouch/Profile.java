package com.nipe.braintouch;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Nils Berg on 2018-03-09.
 */

public class Profile {

    private final String TAG = "PROFILE";
    private String display_name;
    private byte[] byte_data;

    public Profile(String display_name, byte[] byte_data){
        this.display_name = display_name;
        this.byte_data = byte_data;
        int length = (byte_data != null) ? byte_data.length : -1;
        Log.d(TAG, "Profile: "+ display_name + " got dataLength: " + String.valueOf(length));
    }
    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public byte[] getByte_data() {
        return byte_data;
    }

    public void setByte_data(byte[] byte_data) {
        this.byte_data = byte_data;
    }
}
