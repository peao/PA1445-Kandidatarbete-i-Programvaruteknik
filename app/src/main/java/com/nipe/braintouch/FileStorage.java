package com.nipe.braintouch;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Nils Berg on 2018-03-09.
 */

public class FileStorage {
    private final String TAG = "FILESTORAGE";

    private static FileStorage instance = null;
    private static FileOutputStream outputStream;
    private final static String dirNname = "profiles";
    private File directory;
    private ArrayList<File> files;
    private Context mContext;
    private static ArrayList<Profile> allProfiles;
    private boolean needsToUpdate = true;

    protected FileStorage(Context context){
        mContext = context;
        allProfiles = new ArrayList<>();
        directory = new File(mContext.getFilesDir() + "/" + dirNname);
        directory.mkdir();
        files = new ArrayList<>();
        File[] tempArr = directory.listFiles();
        for (File f : tempArr) {
            files.add(f);
        }

        //Log.d(TAG, "File: " + String.valueOf(file.isFile()));
    }

    public static FileStorage getInstance(Context context) {
        if(instance == null) {
            instance = new FileStorage(context);
        }
        return instance;
    }

    public List<Profile> getAllProfiles(){
        if(needsToUpdate){
            allProfiles.clear();
            files.clear();

            File[] tempArr = directory.listFiles();
            for (File f : tempArr) {
                files.add(f);
            }
            try {
                for (File f: files) {
                    String name = f.getName();
                    byte[] bytes = org.apache.commons.io.FileUtils.readFileToByteArray(f);
                    if(bytes.length < 2) bytes = null;
                    allProfiles.add(new Profile(name, bytes));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            needsToUpdate = false;
        }

        return allProfiles;
    }

    public void insertProfile(Profile p){
        try {
            allProfiles.add(p);
            File f = new File(directory.getAbsolutePath() +  "/" + p.getDisplay_name());
            f.createNewFile();
            files.add(f);
            org.apache.commons.io.FileUtils.writeByteArrayToFile(f, p.getByte_data());

            needsToUpdate = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getNowAsString() {
        Date now = Calendar.getInstance().getTime();
        return DateFormat.getDateInstance(DateFormat.SHORT).format(now);
    }

    public void logForProfile(Profile p, String logMsg) {
        if (p == null) return;
        try {
            File f = new File(directory.getAbsolutePath() + "/" + p.getDisplay_name() + "_log");
            if(!f.exists()) f.createNewFile();

            BufferedWriter writer = new BufferedWriter(new FileWriter(f, true));
            writer.write(getNowAsString() + " " + logMsg + "\n");
            writer.close();
            Log.d(TAG, "Logged: " + logMsg + " to file: " + directory.getAbsolutePath() + "/" + p.getDisplay_name() + "_log");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}