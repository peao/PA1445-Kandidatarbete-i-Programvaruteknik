package com.nipe.braintouch;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.emotiv.bluetooth.Emotiv;
import com.emotiv.sdk.IEE_Event_t;
import com.emotiv.sdk.IEE_FacialExpressionAlgo_t;
import com.emotiv.sdk.IEE_FacialExpressionEvent_t;
import com.emotiv.sdk.IEE_FacialExpressionTrainingControl_t;
import com.emotiv.sdk.IEE_InputChannels_t;
import com.emotiv.sdk.IEE_MentalCommandAction_t;
import com.emotiv.sdk.IEE_MentalCommandEvent_t;
import com.emotiv.sdk.IEE_MentalCommandTrainingControl_t;
import com.emotiv.sdk.SWIGTYPE_p_int;
import com.emotiv.sdk.SWIGTYPE_p_unsigned_int;
import com.emotiv.sdk.SWIGTYPE_p_void;
import com.emotiv.sdk.edkJava;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import static com.emotiv.sdk.IEE_FacialExpressionAlgo_t.FE_NEUTRAL;
import static com.emotiv.sdk.IEE_FacialExpressionAlgo_t.FE_WINK_RIGHT;
import static com.emotiv.sdk.edkJava.IEE_EmoEngineEventGetEmoState;
import static com.emotiv.sdk.edkJava.IEE_EngineDisconnect;
import static com.emotiv.sdk.edkJava.IEE_FacialExpressionSetTrainingAction;
import static com.emotiv.sdk.edkJava.IEE_FacialExpressionSetTrainingControl;
import static com.emotiv.sdk.edkJava.IEE_GetBaseProfile;
import static com.emotiv.sdk.edkJava.IEE_GetUserProfile;
import static com.emotiv.sdk.edkJava.IEE_GetUserProfileBytes;
import static com.emotiv.sdk.edkJava.IEE_GetUserProfileSize;
import static com.emotiv.sdk.edkJava.IEE_MentalCommandSetActiveActions;
import static com.emotiv.sdk.edkJava.IEE_MentalCommandSetTrainingAction;
import static com.emotiv.sdk.edkJava.IEE_MentalCommandSetTrainingControl;
import static com.emotiv.sdk.edkJava.IEE_ProfileEventCreate;
import static com.emotiv.sdk.edkJava.IEE_SetUserProfile;
import static com.emotiv.sdk.edkJava.IS_FacialExpressionGetLowerFaceAction;
import static com.emotiv.sdk.edkJava.IS_FacialExpressionGetLowerFaceActionPower;
import static com.emotiv.sdk.edkJava.IS_FacialExpressionGetUpperFaceAction;
import static com.emotiv.sdk.edkJava.IS_FacialExpressionGetUpperFaceActionPower;
import static com.emotiv.sdk.edkJava.IS_FacialExpressionIsBlink;
import static com.emotiv.sdk.edkJava.IS_GetBatteryChargeLevel;
import static com.emotiv.sdk.edkJava.IS_GetContactQuality;
import static com.emotiv.sdk.edkJava.IS_MentalCommandGetCurrentAction;
import static com.emotiv.sdk.edkJava.IS_MentalCommandGetCurrentActionPower;
import static com.emotiv.sdk.edkJava.uint_p_value;
import static com.emotiv.sdk.edkJavaConstants.EDK_OK;

public class HeadsetConnector {
    private static final String TAG = "HeadsetConnector";
    private static final String TAG_EXPERIMENT = "HC_E";
    private final int REQUEST_ENABLE_BT = 0;
    private final int NR_OF_TIRES_TO_CONNECT = 10;

    private Timer timer;
    private TimerTask eegPoller;

    private HashMap<Integer, String> edkStatusMap, mcActionMap;
    private static Context context;
    private static boolean isConnected;
    private BluetoothAdapter btAdapter;
    private MouseHandler mh;

    static private FileStorage fs = FileStorage.getInstance(context);
    static private Profile profileInUse;

    private static SWIGTYPE_p_void emoState;
    private SWIGTYPE_p_void handleEvent, motionDataHandle, profile;
    private static SWIGTYPE_p_unsigned_int userID;
    private IEE_MentalCommandAction_t prevMCA;
    private int previousState = -9999;

    private final static int TRAIN_MC_NEUTRAL = 0;
    private final static int TRAIN_PUSH = 1;
    private final static int TRAIN_PULL = 2;
    private final static int TRAIN_RIGHT = 3;
    private final static int TRAIN_LEFT = 4;

    private final static int TRAIN_BLINK = 5;
    private final static int TRAIN_RIGHT_WINK = 6;
    private final static int TRAIN_LEFT_WINK = 7;
    private final static int TRAIN_LEFT_SMIRK = 8;
    private final static int TRAIN_RIGHT_SMIRK = 9;
    private final static int TRAIN_FE_NEUTRAL = 10;
    private final static int TRAIN_SMILE = 11;
    private final static int TRAIN_FROWN = 12;
    private final static int TRAIN_CLENCH = 13;

    private long lastTimeSinceClick;
    private final int clickInterval = 300;

    private static long[] mentalAction = { -1l, -1l, -1l, -1l};
    private static IEE_MentalCommandAction_t trainMC;
    private static HashMap<IEE_MentalCommandAction_t, Integer> trainingCountMC;

    public HeadsetConnector(Context context) {
        Log.d(TAG,"In HeadsetConnector constructor");

        this.context = context;
        isConnected = false;

        //initBluetooth();
        initEmotive();

        initTimerTask();
        timer = new Timer();
        timer.schedule(eegPoller, 10, 10);
        lastTimeSinceClick = System.currentTimeMillis();

        userID = edkJava.new_uint_p();

        trainMC = IEE_MentalCommandAction_t.MC_NEUTRAL;
        trainingCountMC = new HashMap<>();
    }

    private void initTimerTask() {
        if(eegPoller != null) return;
        eegPoller = new TimerTask() {
            @Override
            public void run() {
                try {
                    updateLoop();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    private void initEmotive() {
        Log.d(TAG,"initEmotive");
        Emotiv.IEE_EmoInitDevice(context);
        setUpEDKStateMap();

        Log.d(TAG, "Connecting to engine - IEE_EngineConnect");
        int result = -999;
        for(int i = 0; i < NR_OF_TIRES_TO_CONNECT; ++i) {
          result = edkJava.IEE_EngineConnect("Emotiv Systems-5");
            if(result == EDK_OK) {
                Log.d(TAG, "Connected to engine");
                i = NR_OF_TIRES_TO_CONNECT;
            }
            else {
                Log.d(TAG, "Try: " + String.valueOf(i) + " Failed to connect to engine with status: " + edkStatusMap.get(result));
            }
        }

        if (result == EDK_OK) {
            handleEvent = edkJava.IEE_EmoEngineEventCreate();
            Log.d(TAG, "handleEvent: " + handleEvent.toString());

            emoState = edkJava.IEE_EmoStateCreate();
            Log.d(TAG, "emoState:" + String.valueOf(emoState));

            motionDataHandle = edkJava.IEE_MotionDataCreate();

            profile = IEE_ProfileEventCreate();
            IEE_GetBaseProfile(profile);
        }
    }

    private void initBluetooth() {
        Log.d(TAG,"initBluetooth");
        final BluetoothManager btManager = (BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
    }

    private void updateLoop() {
        int state = edkJava.IEE_EngineGetNextEvent(handleEvent);
        if (state != previousState) {
            //Log.d(TAG, "EventState: " + edkStatusMap.get(state));
            previousState = state;
        }

        if (state == EDK_OK) {
            IEE_Event_t eventType = edkJava.IEE_EmoEngineEventGetType(handleEvent);
            //Log.d(TAG, "EventType: " + eventType.name());

            switch (eventType) {
                case IEE_UserAdded:
                    Log.d(TAG, "User added");
                    int result = edkJava.IEE_EmoEngineEventGetUserId(handleEvent, userID);
                    Log.d(TAG, "IEE_EmoEngineEventGetUserId: " + edkStatusMap.get(result));
                    break;
                case IEE_EmoStateUpdated:
                    if(IEE_EmoEngineEventGetEmoState(handleEvent, emoState) == EDK_OK) {

                        /*IEE_MentalCommandAction_t action = IS_MentalCommandGetCurrentAction(emoState);
                        if (action != prevMCA) {
                            prevMCA = action;
                            Log.d(TAG, "MentalCommandAction: " + action.toString());
                        }*/

                        /*float clench = IS_FacialExpressionGetClenchExtent(emoState);
                        if (clench > 0.0) Log.d(TAG + "FE", "clench: " + String.valueOf(clench));

                        //float eyebrow = IS_FacialExpressionGetEyebrowExtent(emoState);
                        //if(eyebrow > 0.0) Log.d(TAG + "FE", "eyebrow: " + String.valueOf(eyebrow));

                        float smile = IS_FacialExpressionGetSmileExtent(emoState);
                        if (smile > 0.0) Log.d(TAG + "FE", "smile: " + String.valueOf(smile));

                        float blink = IS_FacialExpressionIsBlink(emoState);
                        if (blink > 0.0) Log.d(TAG + "FE", "blink: " + String.valueOf(blink));

                        int rightWink = IS_FacialExpressionIsRightWink(emoState);
                        if (rightWink == 1) {
                            Log.d(TAG + "FE", "rightWink: " + String.valueOf(rightWink));
                            sendInput("RIGHT");
                        } */

                        /*
                        IEE_FacialExpressionAlgo_t faceExpLower = IS_FacialExpressionGetLowerFaceAction(emoState);
                        float faceExpLowerPower = IS_FacialExpressionGetLowerFaceActionPower(emoState);
                        if(faceExpLower != IEE_FacialExpressionAlgo_t.FE_NEUTRAL)
                            Log.d(TAG + "_FE_LOW", faceExpLower.toString() + " power: " + String.valueOf(faceExpLowerPower));
                        //handleFEAlgo(faceExpLower);

                        IEE_FacialExpressionAlgo_t faceExpUpper = IS_FacialExpressionGetUpperFaceAction(emoState);
                        float faceExpUpperPower = IS_FacialExpressionGetUpperFaceActionPower(emoState);
                        if(faceExpUpper != IEE_FacialExpressionAlgo_t.FE_SURPRISE)
                            Log.d(TAG + "_FE_UP", faceExpUpper.toString() + " power: " + String.valueOf(faceExpUpperPower));
                        //handleFEAlgo(faceExpUpper);
                        */
                        //Log.d(TAG + "_FE", faceExpUpper.toString() + " power: " + String.valueOf(faceExpUpperPower) + "\t" +
                        //                            faceExpLower.toString() + " power: " + String.valueOf(faceExpLowerPower));

                        /*int leftWink = IS_FacialExpressionIsLeftWink(emoState);
                        if (leftWink == 1) {
                            Log.d(TAG + "FE", "leftWink: " + String.valueOf(leftWink));
                            sendInput("LEFT");
                        }

                        int lookingDown = IS_FacialExpressionIsLookingDown(emoState);
                        if (lookingDown == 1)
                            Log.d(TAG + "FE", "lookingDown: " + String.valueOf(lookingDown));

                        int lookingUp = IS_FacialExpressionIsLookingUp(emoState);
                        if (lookingUp == 1)
                            Log.d(TAG + "FE", "lookingUp: " + String.valueOf(lookingUp));

                        int lookingLeft = IS_FacialExpressionIsLookingLeft(emoState);
                        if (lookingLeft == 1)
                            Log.d(TAG + "FE", "lookingLeft: " + String.valueOf(lookingLeft));

                        int lookingRight = IS_FacialExpressionIsLookingRight(emoState);
                        if (lookingRight == 1)
                            Log.d(TAG + "FE", "lookingRight: " + String.valueOf(lookingRight));
                        */

                        int blink = IS_FacialExpressionIsBlink(emoState);
                        if (blink == 1) {
                            if((System.currentTimeMillis() - lastTimeSinceClick) >= clickInterval) {
                                //Log.d(TAG, "FE_BLINK");
                                //fs.logForProfile(profileInUse, "FE_BLINK");
                                //sendInput("CLICK");
                                lastTimeSinceClick = System.currentTimeMillis();
                            }
                        }

                        IEE_MentalCommandAction_t command = IS_MentalCommandGetCurrentAction(emoState);
                        float mentalCommandPower = IS_MentalCommandGetCurrentActionPower(emoState);

                        switch (command) {
                            case MC_PUSH:
                                Log.d(TAG + "MC", "PUSH Power: " + String.valueOf(mentalCommandPower));
                                fs.logForProfile(profileInUse, "MC_PUSH");
                                sendInput("UP");
                                break;
                            case MC_PULL:
                                Log.d(TAG + "MC", "PULL Power: " + String.valueOf(mentalCommandPower));
                                fs.logForProfile(profileInUse, "MC_PULL");
                                sendInput("DOWN");
                                break;
                            case MC_LEFT:
                                Log.d(TAG + "MC", "LEFT Power: " + String.valueOf(mentalCommandPower));
                                //fs.logForProfile(profileInUse, "MC_LEFT");
                                //sendInput("LEFT");
                                break;
                            case MC_RIGHT:
                                Log.d(TAG + "MC", "RIGHT Power: " + String.valueOf(mentalCommandPower));
                                //fs.logForProfile(profileInUse, "MC_RIGHT");
                                //sendInput("RIGHT");
                                break;
                            case MC_NEUTRAL: break;
                            default:
                                Log.d(TAG, command.toString());
                        }

                        break;
                    }
                case IEE_UserRemoved:
                    Log.d(TAG, "User removed");
                    profileInUse = null;
                    break;
                case IEE_MentalCommandEvent:
                    handleMentalCommandEvent(edkJava.IEE_MentalCommandEventGetType(handleEvent));
                    break;
                case IEE_FacialExpressionEvent:
                    handleFacialExpressionEvent(edkJava.IEE_FacialExpressionEventGetType(handleEvent));
                    break;
                default:
                    Log.d(TAG, "Unhandled event");
                    break;
            }
        }
    }

    private void setUpEDKStateMap() {
        edkStatusMap = new HashMap<Integer, String>();
        mcActionMap = new HashMap<Integer, String>();

        try {
            Field[] fields = edkJava.class.getFields();
            String temp = "";
            edkJava test = new edkJava();

            for(int i = 0; i < fields.length; i++) {
                temp = fields[i].getName();
                Integer value = (Integer) fields[i].get(test);
                edkStatusMap.put(value, temp);
                Log.d("edkStatusMap", "value: " + String.valueOf(value) + " is " + temp);
            }
            Log.d(TAG, "Set up done: edkStatusMap");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        IEE_MentalCommandAction_t[] mcActions = IEE_MentalCommandAction_t.values();
        for(int i = 0; i < mcActions.length; i++)
            mcActionMap.put(mcActions[i].ordinal(), mcActions[i].name());
        Log.d(TAG, "Set up done: mcActionMap");

    }

    public static void getEmotivDevices(ArrayList<String> result){
        result.clear();

        int nrOfInsight = Emotiv.IEE_GetInsightDeviceCount();
        Log.d(TAG, "Found " + String.valueOf(nrOfInsight) + " device(s)");

        if (nrOfInsight > 0) {
            for(int i = 0; i < Emotiv.IEE_GetInsightDeviceCount(); i++){
                Log.d(TAG, "Found device: " + Emotiv.IEE_GetInsightDeviceName(i));
                result.add(Emotiv.IEE_GetInsightDeviceName(i));
            }
        }
    }

    public static void connectToDevice(int index){
        String deviceName = Emotiv.IEE_GetInsightDeviceName(index);

        if (!Emotiv.IEE_ConnectInsightDevice(index)) {
            Log.d(TAG, "Failed to connect to headset");
            isConnected = false;
        } else {
            Log.d(TAG,"Connected to: " + deviceName);
            isConnected = true;

            Intent intent = new Intent();
            intent.setAction("HEADSET_CONNECTED");
            context.sendBroadcast(intent);
        }
    }

    public static void killEngineConnection(){
        IEE_EngineDisconnect();
    }

    public static int getConnectedDeviceBatteryLevel() {
        SWIGTYPE_p_int maxCharge, charge;
        charge = edkJava.new_int_p();
        maxCharge = edkJava.new_int_p();

        IS_GetBatteryChargeLevel(emoState,charge,maxCharge);

        int chargeLevel = edkJava.int_p_value(charge);

        edkJava.delete_int_p(charge);
        edkJava.delete_int_p(maxCharge);

        return chargeLevel;
    }

    public static void getConnectedDeviceConnectionLevels(Vector<Integer> strenghts) {
        try {
            strenghts.add(0, IS_GetContactQuality(emoState, IEE_InputChannels_t.IEE_CHAN_AF3).ordinal());
            strenghts.add(1, IS_GetContactQuality(emoState, IEE_InputChannels_t.IEE_CHAN_T7).ordinal());
            strenghts.add(2, IS_GetContactQuality(emoState, IEE_InputChannels_t.IEE_CHAN_Pz).ordinal());
            strenghts.add(3, IS_GetContactQuality(emoState, IEE_InputChannels_t.IEE_CHAN_T8).ordinal());
            strenghts.add(4, IS_GetContactQuality(emoState, IEE_InputChannels_t.IEE_CHAN_AF4).ordinal());
        }catch (IllegalArgumentException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private byte[] getUserProfileInfo() {
        edkJava.IEE_EmoEngineEventGetUserId(handleEvent, userID);
        long uIDValue = edkJava.uint_p_value(userID);
        return getUserProfileInfo(uIDValue);
    }

    public byte[] getUserProfileInfo(long id) {
        // Get profile for user with id = id
        int getUserProfileRes = IEE_GetUserProfile(id, profile);
        //Log.d(TAG, "GetUserProfileInfo: " + String.valueOf(id) + " " + edkStatusMap.get(getUserProfileRes));
        if(getUserProfileRes == EDK_OK) {
            boolean complete = false;
            byte[] profileBuffer = null;
            SWIGTYPE_p_unsigned_int size = edkJava.new_uint_p();
            // Get user profile size
            if(IEE_GetUserProfileSize(profile, size) == EDK_OK) {
                int buffSize = (int)edkJava.uint_p_value(size);
                profileBuffer = new byte[buffSize];
                //Log.d(TAG, "GetUserProfileSize: " + String.valueOf(buffSize));
                // Fill profileBuffer with data
                if(IEE_GetUserProfileBytes(profile, profileBuffer, buffSize) == EDK_OK) {
                    complete = true;
                }
            }

            edkJava.delete_uint_p(size);
            if(profileBuffer != null && complete) return profileBuffer;
        }
        return null;
    }

    private void sendInput(String msg) {
        Intent intent = new Intent();
        intent.setAction("BUTTON_PRESSED");
        intent.putExtra("data", msg);
        context.sendBroadcast(intent);
    }

    private void sendTrainingMessage(String message) {
        Intent intent = new Intent();
        intent.setAction("TRAINING");
        intent.putExtra("data", message);
        context.sendBroadcast(intent);
    }

    public static void setProfile(Profile p) throws Exception {
        byte[] bytes = p.getByte_data();

        if(bytes == null) {
            Log.d(TAG, "USER HAS NO PREVIOUS DATA");
            SWIGTYPE_p_void profileP = IEE_ProfileEventCreate();
            if (IEE_GetBaseProfile(profileP) == EDK_OK) {
                SWIGTYPE_p_unsigned_int size = edkJava.new_uint_p();
                // Get user profile size
                if(IEE_GetUserProfileSize(profileP, size) == EDK_OK) {
                    int buffSize = (int)edkJava.uint_p_value(size);
                    bytes = new byte[buffSize];
                    // Fill profileBuffer with data
                    if(IEE_GetUserProfileBytes(profileP, bytes, buffSize) != EDK_OK) {
                        bytes = null;
                    }
                }
                edkJava.delete_uint_p(size);
            }
        }

        if(bytes == null) throw new Exception("Unable to set profile");

        int result = IEE_SetUserProfile(edkJava.uint_p_value(userID), bytes, bytes.length);
        if(result == EDK_OK) {
            Log.d(TAG, "Profile set");
            Intent intent = new Intent();
            intent.setAction("PROFILE_SET");
            intent.putExtra("data", p.getDisplay_name());
            context.sendBroadcast(intent);
            profileInUse = p;

            long push = IEE_MentalCommandAction_t.MC_PUSH.swigValue();
            long pull = IEE_MentalCommandAction_t.MC_PULL.swigValue();
            //long left = IEE_MentalCommandAction_t.MC_LEFT.swigValue();
            //long right = IEE_MentalCommandAction_t.MC_RIGHT.swigValue();
            long listActions = push | pull; // | left | right;
            //Log.d(TAG, "MentalCommandSetActiveActions: " + String.valueOf(edkJava.IEE_MentalCommandSetActiveActions(edkJava.uint_p_value(userID), listActions)));
        }
        else {
            Log.d(TAG, "Profile failed to be set with error: " + String.valueOf(result));
        }
    }

    public static void startTraining(int TO_TRAIN_CODE, boolean reset) {
        long user_id = uint_p_value(userID);
        IEE_MentalCommandAction_t to_train_mental = IEE_MentalCommandAction_t.MC_NEUTRAL;
        IEE_FacialExpressionAlgo_t to_train_facial = FE_NEUTRAL;

        switch (TO_TRAIN_CODE){
            case TRAIN_MC_NEUTRAL:
                to_train_mental = IEE_MentalCommandAction_t.MC_NEUTRAL;
                break;

            case TRAIN_PUSH:
                to_train_mental = IEE_MentalCommandAction_t.MC_PUSH;
                break;

            case TRAIN_PULL:
                to_train_mental = IEE_MentalCommandAction_t.MC_PULL;
                break;

            case TRAIN_RIGHT:
                to_train_mental = IEE_MentalCommandAction_t.MC_RIGHT;
                break;

            case TRAIN_LEFT:
                to_train_mental = IEE_MentalCommandAction_t.MC_LEFT;
                break;

            case TRAIN_BLINK:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_BLINK;
                break;

            case TRAIN_RIGHT_WINK:
                to_train_facial = FE_WINK_RIGHT;
                break;

            case TRAIN_LEFT_WINK:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_WINK_LEFT;
                break;

            case TRAIN_LEFT_SMIRK:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_SMIRK_LEFT;
                break;

            case TRAIN_RIGHT_SMIRK:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_SMIRK_RIGHT;
                break;

            case TRAIN_FE_NEUTRAL:
                to_train_facial = FE_NEUTRAL;
                break;
            case TRAIN_SMILE:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_SMILE;
                break;
            case TRAIN_FROWN:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_FROWN;
                break;
            case TRAIN_CLENCH:
                to_train_facial = IEE_FacialExpressionAlgo_t.FE_CLENCH;
                break;
            default:
                to_train_mental = IEE_MentalCommandAction_t.MC_NEUTRAL;
                to_train_facial = FE_NEUTRAL;
                break;
        }


        if(TO_TRAIN_CODE < 5) {
            if(to_train_mental != IEE_MentalCommandAction_t.MC_NEUTRAL) addMentalAction(to_train_mental);
            setMentalActions();
            IEE_MentalCommandSetTrainingAction(user_id, to_train_mental);
            trainMC = to_train_mental;

            if(reset) {
                Log.d(TAG,"Reset training: " + to_train_mental.toString());
                fs.logForProfile(profileInUse, "Reset training: " + to_train_mental.toString());
                IEE_MentalCommandSetTrainingControl(user_id, IEE_MentalCommandTrainingControl_t.MC_RESET);
            } else {
                Log.d(TAG,"Start training: " + to_train_mental.toString());
                fs.logForProfile(profileInUse, "Start training: " + to_train_mental.toString());
                IEE_MentalCommandSetTrainingControl(user_id, IEE_MentalCommandTrainingControl_t.MC_START);
            }
        } else {
            IEE_FacialExpressionSetTrainingAction(user_id, to_train_facial);
            if(reset) {
                Log.d(TAG,"Reset training: " + to_train_facial.toString());
                fs.logForProfile(profileInUse, "Reset training: " + to_train_facial.toString());
                IEE_FacialExpressionSetTrainingControl(user_id, IEE_FacialExpressionTrainingControl_t.FE_RESET);
            } else {
                Log.d(TAG,"Start training: " + to_train_facial.toString());
                fs.logForProfile(profileInUse, "Start training: " + to_train_facial.toString());
                IEE_FacialExpressionSetTrainingControl(user_id, IEE_FacialExpressionTrainingControl_t.FE_START);
            }
        }
    }

    public static void trainingResult(boolean accepted, int TRAIN_CODE) {
        Log.d(TAG,"Received training result " + String.valueOf(accepted));

        if(TRAIN_CODE < 5) {
            IEE_MentalCommandTrainingControl_t result = IEE_MentalCommandTrainingControl_t.MC_REJECT;
            if(accepted){
                result = IEE_MentalCommandTrainingControl_t.MC_ACCEPT;
            }
            IEE_MentalCommandSetTrainingControl(uint_p_value(userID), result);
        } else {
            IEE_FacialExpressionTrainingControl_t result = IEE_FacialExpressionTrainingControl_t.FE_REJECT;
            if(accepted){
                result = IEE_FacialExpressionTrainingControl_t.FE_ACCEPT;
            }
            IEE_FacialExpressionSetTrainingControl(uint_p_value(userID), result);
        }
    }

    public static void persistCurrentProfileToDB() {
        fs.insertProfile(profileInUse);
    }

    private void handleMentalCommandEvent(IEE_MentalCommandEvent_t event) {
        switch(event) {
            case IEE_MentalCommandTrainingStarted:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingStarted");
                sendTrainingMessage("STARTED");
                break;
            case IEE_MentalCommandTrainingSucceeded:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingSucceeded");
                fs.logForProfile(profileInUse, "Training: SUCCESS");
                sendTrainingMessage("SUCCESS");
                break;
            case IEE_MentalCommandTrainingReset:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingReset");
                break;
            case IEE_MentalCommandTrainingRejected:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingRejected");
                fs.logForProfile(profileInUse, "Training: REJECTED");
                break;
            case IEE_MentalCommandTrainingFailed:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingFailed");
                fs.logForProfile(profileInUse, "Training: FAILED");
                sendTrainingMessage("FAILED");
                break;
            case IEE_MentalCommandTrainingDataErased:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingDataErased");
                break;
            case IEE_MentalCommandTrainingCompleted:
                Log.d(TAG, "MentalCommandEvent: MentalCommandTrainingCompleted");
                profileInUse.setByte_data(getUserProfileInfo());
                Log.d(TAG, "Bytes: " + String.valueOf(profileInUse.getByte_data().length));
                persistCurrentProfileToDB();
                if(!trainingCountMC.containsKey(trainMC))
                    trainingCountMC.put(trainMC, 1);
                else
                    trainingCountMC.put(trainMC, (trainingCountMC.get(trainMC) + 1));
                //long push = IEE_MentalCommandAction_t.MC_PUSH.swigValue();
                //long pull = IEE_MentalCommandAction_t.MC_PULL.swigValue();
                //long left = IEE_MentalCommandAction_t.MC_LEFT.swigValue();
                //long right = IEE_MentalCommandAction_t.MC_RIGHT.swigValue();
                //long listActions = push | pull; //| left | right;
                //edkJava.IEE_MentalCommandSetActiveActions(uint_p_value(userID), listActions);
                Log.d(TAG + "_TRAIN", trainingCountMC.toString());
                break;
            case IEE_MentalCommandSignatureUpdated:
                Log.d(TAG, "MentalCommandEvent: MentalCommandSignatureUpdated");
                break;
            case IEE_MentalCommandAutoSamplingNeutralCompleted:
                Log.d(TAG, "MentalCommandEvent: MentalCommandAutoSamplingNeutralCompleted");
                break;
            case IEE_MentalCommandNoEvent:
                Log.d(TAG, "MentalCommandEvent: MentalCommandNoEvent");
                break;
            default:
                Log.d(TAG, "MentalCOmmandEvent: " + event.toString());
        }
    }

    private void handleFacialExpressionEvent(IEE_FacialExpressionEvent_t event){
        switch (event) {
            case IEE_FacialExpressionTrainingStarted:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingStarted");
                sendTrainingMessage("STARTED");
                break;
            case IEE_FacialExpressionTrainingSucceeded:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingSucceeded");
                sendTrainingMessage("SUCCESS");
                break;
            case IEE_FacialExpressionTrainingReset:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingReset");
                break;
            case IEE_FacialExpressionTrainingRejected:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingRejected");
                break;
            case IEE_FacialExpressionTrainingFailed:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingFailed");
                sendTrainingMessage("FAILED");
                break;
            case IEE_FacialExpressionTrainingDataErased:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingDataErased");
                break;
            case IEE_FacialExpressionTrainingCompleted:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionTrainingCompleted");
                profileInUse.setByte_data(getUserProfileInfo());
                Log.d(TAG, "Bytes: " + String.valueOf(profileInUse.getByte_data().length));
                persistCurrentProfileToDB();
                break;
            case IEE_FacialExpressionNoEvent:
                Log.d(TAG, "FacialExpressionEvent: FacialExpressionNoEvent");
                break;
            default:
        }

    }

    private void handleFEAlgo(IEE_FacialExpressionAlgo_t faceExp) {
        switch(faceExp) {
            case FE_BLINK:
                break;
            case FE_CLENCH:
                sendInput("DOWN");
                break;
            case FE_FROWN:
                sendInput("UP");
                break;
            case FE_HORIEYE:
                break;
            case FE_LAUGH:
                break;
            case FE_NEUTRAL:
                break;
            case FE_SMILE:
                break;
            case FE_SMIRK_LEFT:
                sendInput("LEFT");
                break;
            case FE_SMIRK_RIGHT:
                sendInput("RIGHT");
                break;
            case FE_SURPRISE:
                break;
            case FE_WINK_LEFT:
                break;
            case FE_WINK_RIGHT:
                sendInput("CLICK");
                break;
        }
    }

    private static void addMentalAction(IEE_MentalCommandAction_t action) {
        long swigValue = action.swigValue();
        for (int i = 0; i < mentalAction.length; i++) {
            if(mentalAction[i] == -1) {
                Log.d(TAG, "Mental action: " + action.toString() + " added!");
                mentalAction[i] = swigValue;
                i = mentalAction.length;
            } else if (mentalAction[i] == swigValue) {
                i = mentalAction.length;
            }
        }
    }

    private static void setMentalActions() {
        int nrOfActions = 0;
        for (int i = 0; i < mentalAction.length; i++) {
            if (mentalAction[i] != -1) nrOfActions++;
        }

        long listActions = -1;
        switch (nrOfActions) {
            case 1:
                Log.d(TAG, "1 mental action set");
                listActions = mentalAction[0];
                break;
            case 2:
                Log.d(TAG, "2 mental action set");
                listActions = mentalAction[0] | mentalAction [1];
                break;
            case 3:
                Log.d(TAG, "3 mental action set");
                listActions = mentalAction[0] | mentalAction[1] | mentalAction[2];
                break;
            case 4:
                Log.d(TAG, "4 mental action set");
                listActions = mentalAction[0] | mentalAction[1] | mentalAction[2] | mentalAction[3];
                break;
        }
        if (listActions != -1) {
            edkJava.IEE_MentalCommandSetActiveActions(uint_p_value(userID), listActions);
        }
    }
}
