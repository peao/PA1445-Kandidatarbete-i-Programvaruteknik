package com.nipe.braintouch;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class MouseRenderer {

    private static final int CURSOR_WIDTH = 125;
    private static final int CURSOR_HEIGHT = 125;

    private View topLeftView;
    private ImageView cursorImage;
    private WindowManager wm;

    public MouseRenderer(Context context, int startX, int startY) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        cursorImage = new ImageView(context);
        cursorImage.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.cursor_dot, null));
        cursorImage.setColorFilter(Color.RED);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.TOP;
        params.x = startX;
        params.y = startY;
        params.width = CURSOR_WIDTH;
        params.height = CURSOR_HEIGHT;
        params.alpha = 1f;
        params.horizontalMargin = 0f;
        params.verticalMargin = 0f;

        wm.addView(cursorImage, params);

        topLeftView = new View(context);
        WindowManager.LayoutParams topLeftParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_SYSTEM_ALERT, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, PixelFormat.TRANSLUCENT);
        topLeftParams.gravity = Gravity.LEFT | Gravity.TOP;
        topLeftParams.x = 0;
        topLeftParams.y = 0;
        topLeftParams.width = 0;
        topLeftParams.height = 0;
        wm.addView(topLeftView, topLeftParams);

    }

    public void renderAt(int x, int y) {

        String log = String.valueOf(x) + " | " + String.valueOf(y);
        Log.d("XXX - render",log);
        WindowManager.LayoutParams params = (WindowManager.LayoutParams) cursorImage.getLayoutParams();

        params.x = x;
        params.y = y;

        wm.updateViewLayout(cursorImage, params);
    }

    public void dismantle(){
        this.wm.removeView(this.cursorImage);
    }

    public int getCursorWidth(){
        return CURSOR_WIDTH;
    }

    public int getCursorHeight() {
        return CURSOR_HEIGHT;
    }
}
