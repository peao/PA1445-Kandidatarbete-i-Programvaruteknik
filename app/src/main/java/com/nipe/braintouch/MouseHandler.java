package com.nipe.braintouch;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class MouseHandler {

    private static final String TAG = "MouseHandler";

    private MouseRenderer mr;
    private InputDelegator id;

    private int mouseXPosition;
    private int mouseYPosition;

    private int maxX = 0;
    private int maxY = 0;

    private final int MOVE_SPEED = 50;

    public MouseHandler(Context context, int startXPos, int startYPos) {
        Log.d(TAG,"In MouseHandler constructor");

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        maxX = size.x;
        maxY = size.y;
        Log.d(TAG, "Window w: " + String.valueOf(maxX) + " h: " + String.valueOf(maxY));

        mouseXPosition = startXPos;
        mouseYPosition = startYPos;

        mr = new MouseRenderer(context, mouseXPosition, mouseYPosition);
        id = new InputDelegator(context);
        //id.delayedClick(mouseXPosition + (mr.getCursorWidth() / 2), mouseYPosition + (mr.getCursorHeight() / 2));
    }

    public void setPos(final int x, final int y) {
        mouseXPosition = x - mr.getCursorWidth();
        mouseYPosition = y + mr.getCursorHeight();
        mr.renderAt(mouseXPosition,mouseYPosition);
        Log.d(TAG, "Moved mouse to pos x: " + String.valueOf(mouseXPosition) + " y: " + String.valueOf(mouseYPosition));
    }

    public void moveUp(){
        mouseYPosition -= MOVE_SPEED;
        if(mouseYPosition - (mr.getCursorHeight() / 2) < 0) mouseYPosition = mr.getCursorHeight() / 2;
        mr.renderAt(mouseXPosition,mouseYPosition);
        Log.d(TAG, "UP mouse pos at x: " + String.valueOf(mouseXPosition) + " y: " + String.valueOf(mouseYPosition));
    }

    public void moveDown(){
        mouseYPosition += MOVE_SPEED;
        if(mouseYPosition + (mr.getCursorHeight()/2) > maxY) mouseYPosition = maxY - mr.getCursorHeight()/2;
        mr.renderAt(mouseXPosition,mouseYPosition);
        Log.d(TAG, "DOWN mouse pos at x: " + String.valueOf(mouseXPosition) + " y: " + String.valueOf(mouseYPosition));
    }

    public void moveLeft(){
        mouseXPosition -= MOVE_SPEED;
        if(mouseXPosition - (mr.getCursorWidth() / 2) < 0) mouseXPosition = mr.getCursorWidth() / 2;
        mr.renderAt(mouseXPosition,mouseYPosition);
        Log.d(TAG, "LEFT mouse pos at x: " + String.valueOf(mouseXPosition) + " y: " + String.valueOf(mouseYPosition));
    }

    public void moveRight(){
        mouseXPosition += MOVE_SPEED;
        if(mouseXPosition + (mr.getCursorWidth()/2) > maxX) mouseXPosition = maxX - mr.getCursorWidth() / 2;
        mr.renderAt(mouseXPosition,mouseYPosition);
        Log.d(TAG, "RIGHT mouse pos at x: " + String.valueOf(mouseXPosition) + " y: " + String.valueOf(mouseYPosition));
    }

    public void generateClick(){
        id.click(mouseXPosition + (mr.getCursorWidth() / 2), mouseYPosition + (mr.getCursorHeight() / 2));
    }

    public int getPointerYPos() {
        return (this.mouseYPosition - mr.getCursorHeight());
    }

    public void dismantle(){
        mr.dismantle();
    }
}
