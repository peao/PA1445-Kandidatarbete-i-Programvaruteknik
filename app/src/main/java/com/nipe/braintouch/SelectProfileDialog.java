package com.nipe.braintouch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import java.util.List;
import java.util.Random;

/**
 * Created by Nils Berg on 2018-03-02.
 */

public class SelectProfileDialog extends ListDialog {
    private final String TAG = "SelectProfileDialog";

    private FileStorage fs;
    private final String[] adjectives = {"Smoky","Dangerous","Wise","Fast","Sly","Unaccounted","Chemical","Zazzy","Earthy","Draconian"};
    private final String[] animals = {"Bear","Snake","Fox","Dog","Cat","Bird","Fish","Elephant","Cheetah","Rhino","T-Rex"};

    protected SelectProfileDialog(@NonNull Context context) {
        super(context);

        Button b = content.findViewById(R.id.actionButton);
        b.setText("New profile");
        fs = FileStorage.getInstance(context);
        updateList();
    }

    protected void updateList(){
        items.clear();
        List<Profile> dbItems = fs.getAllProfiles();

        for(Profile p:dbItems){
            items.add(p.getDisplay_name());
        }

        adapter.notifyDataSetChanged();
    }

    protected void updateView(){
        results.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Profile p = fs.getAllProfiles().get(i);
                    HeadsetConnector.setProfile(p);
                }catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });

        content.findViewById(R.id.actionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewProfile();
                updateList();
            }
        });
    }

    private void createNewProfile(){
        Random rand = new Random();
        int an = rand.nextInt(animals.length);
        int ad = rand.nextInt(adjectives.length);

        String profileName = adjectives[ad] + " " + animals[an];
        Profile p = new Profile(profileName,null);
        fs.insertProfile(p);
    }
}