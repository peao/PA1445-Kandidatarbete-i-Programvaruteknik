package com.nipe.braintouch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Nils Berg on 2018-03-22.
 */

public class TrainingCourseActivity extends AppCompatActivity {

    private final String TAG = "TrainingCourseActivity";
    private ArrayList<TextView> grey_zones = new ArrayList<>();
    private ArrayList<TextView> green_zones = new ArrayList<>();

    private MouseHandler mh;
    private BroadcastReceiver receiver;

    private Timer t = new Timer();

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0:
                    pollPointerInArea();
                    break;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_course);

        mh = new MouseHandler(this,500,0);

        findViewById(R.id.trainView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    mh.setPos((int)motionEvent.getX(), (int)motionEvent.getY());
                }
                return true;
            }
        });


        grey_zones.add((TextView) findViewById(R.id.grey_0));
        grey_zones.add((TextView) findViewById(R.id.grey_1));
        grey_zones.add((TextView) findViewById(R.id.grey_2));
        grey_zones.add((TextView) findViewById(R.id.grey_3));

        green_zones.add((TextView) findViewById(R.id.area_1));
        green_zones.add((TextView) findViewById(R.id.area_2));
        green_zones.add((TextView) findViewById(R.id.area_3));

        IntentFilter filter = new IntentFilter("BUTTON_PRESSED");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG,intent.getExtras().getString("data"));

                String input = intent.getExtras().getString("data");
                if(input.equals("UP")){
                    mh.moveUp();
                } else if(input.equals("DOWN")){
                    mh.moveDown();
                } else if(input.equals("LEFT")){
                    mh.moveLeft();
                } else if(input.equals("RIGHT")) {
                    mh.moveRight();
                }  else if(input.equals("CLICK")) {
                    mh.generateClick();
                }
            }
        };

        this.registerReceiver(receiver,filter);

        t.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.obtainMessage(0).sendToTarget();
            }
        },3000,500);
    }

    private void pollPointerInArea() {
        String result = "";
        for(TextView tv: grey_zones){
            if(checkPointerInArea(tv.getY(),tv.getHeight())){
                //pekaren är i en gråzon
                //tv.setBackgroundColor(Color.parseColor("#FF0000"));
            }
        }

        for(TextView tv: green_zones){
            if(checkPointerInArea(tv.getY(),tv.getHeight())){
                tv.setBackgroundColor(Color.parseColor("#00FF00"));
            } else {
                tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        Log.d(TAG,result);

        //mh.moveDown();
    }

    public boolean checkPointerInArea(float y, int height){
//        Log.d(TAG,"Y: " + String.valueOf(y));
//        Log.d(TAG,"Height: " + String.valueOf(height));

        boolean inArea = false;
        int pointerY = mh.getPointerYPos();
        if(pointerY >= y && pointerY <= (y + height)){
            inArea = true;
        }
        return inArea;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mh.dismantle();
        unregisterReceiver(receiver);
        t.cancel();
        t.purge();
    }
}
