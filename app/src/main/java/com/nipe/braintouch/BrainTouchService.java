package com.nipe.braintouch;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import com.nipe.braintouch.MouseHandler;
import com.nipe.braintouch.HeadsetConnector;

public class BrainTouchService extends Service {

    private static final String TAG = "BrainTouchService";

    private HeadsetConnector hc;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG,"In service onCreate");


        hc = new HeadsetConnector(this);
    }
}
