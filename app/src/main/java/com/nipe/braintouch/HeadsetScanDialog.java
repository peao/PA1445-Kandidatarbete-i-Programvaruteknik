package com.nipe.braintouch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

/**
 * Created by Nils Berg on 2018-03-02.
 */

public class HeadsetScanDialog extends ListDialog {

    protected HeadsetScanDialog(@NonNull Context context) {
        super(context);
    }

    protected void updateList(){
        HeadsetConnector.getEmotivDevices(items);
        adapter.notifyDataSetChanged();
    }

    protected void updateView(){
        results.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HeadsetConnector.connectToDevice(i);
            }
        });

        content.findViewById(R.id.actionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Scanning",Toast.LENGTH_SHORT).show();
                updateList();
            }
        });
    }
}