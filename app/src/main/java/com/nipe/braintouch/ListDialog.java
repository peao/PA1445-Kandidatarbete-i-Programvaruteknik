package com.nipe.braintouch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Nils Berg on 2018-03-02.
 */

public abstract class ListDialog extends AlertDialog.Builder {

    protected ListView results;
    protected ArrayAdapter adapter;
    protected final ArrayList<String> items;
    protected View content;

    protected ListDialog(@NonNull Context context) {
        super(context);

        items = new ArrayList<>();

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.content = inflater.inflate(R.layout.scan_list,null);

        results = content.findViewById(R.id.scanList);
        adapter = new ArrayAdapter(getContext(), R.layout.scan_list_item, items);
        results.setAdapter(adapter);

        updateView();
        setView(content);
    }

    @Override
    public AlertDialog show() {
        final AlertDialog dialog = super.show();

        content.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    abstract protected void updateView();
    abstract protected void updateList();
}