package com.nipe.braintouch;

import android.app.Instrumentation;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;

import static java.lang.Thread.sleep;

public class InputDelegator {

    public InputDelegator(Context context) {

    }

    public void click(final int clickX, final int clickY) {

        String log = String.valueOf(clickX) + " | " + String.valueOf(clickY);
        Log.d("XXX - click", log);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Instrumentation instrumentation = new Instrumentation();
                instrumentation.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, clickX, clickY, 0));
                instrumentation.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, clickX, clickY, 0));
            }
        }).start();
    }

    public void delayedClick(final int clickX, final int clickY) {
        String log = String.valueOf(clickX) + " | " + String.valueOf(clickY);
        Log.d("XXX - click", log);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Instrumentation instrumentation = new Instrumentation();
                instrumentation.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, clickX, clickY, 0));
                instrumentation.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, clickX, clickY, 0));
            }
        }).start();
    }
}
